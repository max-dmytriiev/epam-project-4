<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<t:wrapper>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <table class="table table-striped">
                    <tr>
                        <th><fmt:message key="admin.users.table.caption.login"/></th>
                        <th><fmt:message key="admin.users.table.caption.fname"/></th>
                        <th><fmt:message key="admin.users.table.caption.lname"/></th>
                        <th><fmt:message key="admin.users.table.caption.status"/></th>
                        <th><fmt:message key="admin.users.table.caption.actions"/></th>
                    </tr>
                    <c:forEach items="${requestScope.users}" var="usr">
                        <tr>
                            <td>${usr.login}</td>
                            <td>${usr.firstName}</td>
                            <td>${usr.lastName}</td>
                            <td>${usr.permissionLevel}</td>
                            <td>
                                <c:if test="${usr.permissionLevel eq \"COMMON\"}">
                                    <form action="${pageContext.request.contextPath}/shop" method="post">
                                        <input type="hidden" name="command" value="BLOCK_USER"/>
                                        <input type="hidden" name="id" value="${usr.id}"/>
                                        <button type="submit" class="btn-danger"><fmt:message key="admin.users.block"/></button>
                                    </form>
                                </c:if>

                                <c:if test="${usr.permissionLevel eq \"BLACKLIST\"}">
                                    <form action="${pageContext.request.contextPath}/shop" method="post">
                                        <input type="hidden" name="command" value="UNBLOCK_USER"/>
                                        <input type="hidden" name="id" value="${usr.id}"/>
                                        <button type="submit" class="btn-success"><fmt:message key="admin.users.unblock"/></button>
                                    </form>
                                </c:if>

                            </td>
                        </tr>
                    </c:forEach>
                </table>
                <form action="${pageContext.request.contextPath}/shop" method="post">
                    <input type="hidden" name="command" value="HOME"/>
                    <button type="submit" class="btn btn-default">&lt;&lt; <fmt:message key="admin.users.back"/></button>
                </form>
            </div>
            <div class="col-md-4">
                <h5><fmt:message key="admin.users.pagenumber"/>: ${requestScope.page}</h5>
                <c:if test="${!empty requestScope.page and requestScope.page gt 0}">
                    <form action="${pageContext.request.contextPath}/shop" method="post" style="display: inline">
                        <input type="hidden" name="command" value="MANAGE_USERS"/>
                        <input type="hidden" name="page" value="${requestScope.page - 1}"/>
                        <input type="submit" value="&lt;&lt;" class="btn btn-default">
                    </form>
                </c:if>
                <c:if test="${!empty requestScope.page and requestScope.page lt requestScope.countPages - 1}">
                    <form action="${pageContext.request.contextPath}/shop" method="post" style="display: inline">
                        <input type="hidden" name="command" value="MANAGE_USERS"/>
                        <input type="hidden" name="page" value="${requestScope.page + 1}"/>
                        <input type="submit" value="&gt;&gt;" class="btn btn-default">
                    </form>
                </c:if>

            </div>
        </div>
    </div>
</t:wrapper>