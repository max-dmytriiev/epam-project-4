<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<t:wrapper>
    <div class="container">
        <div class="row">
            <form action="${pageContext.request.contextPath}/shop" method="post">
                <input type="hidden" name="command" value="MANAGE_USERS">
                <button role="button" type="submit" class="btn btn-default"><fmt:message key="admin.index.manage.users-label"/></button>
            </form>
            <br/>
            <form action="${pageContext.request.contextPath}/shop" method="post">
                <input type="hidden" name="command" value="MANAGE_ITEMS">
                <button role="button" type="submit" class="btn btn-default"><fmt:message key="admin.index.manage.goods-label"/></button>
            </form>
        </div>
    </div>
</t:wrapper>