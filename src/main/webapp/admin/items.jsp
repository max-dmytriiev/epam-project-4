<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<t:wrapper>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <table class="table table-striped">
                    <tr>
                        <th><fmt:message key="admin.items.table.caption.image"/></th>
                        <th><fmt:message key="admin.items.table.caption.id"/></th>
                        <th><fmt:message key="admin.items.table.caption.name"/></th>
                        <th><fmt:message key="admin.items.table.caption.description"/></th>
                        <th><fmt:message key="admin.items.table.caption.price"/></th>
                        <th><fmt:message key="admin.items.table.caption.actions"/></th>
                    </tr>
                    <c:forEach items="${requestScope.items}" var="item">
                        <tr>
                            <td><img src="${item.image}" style="width: 30px"/></td>
                            <td>${item.id}</td>
                            <td>${item.name}</td>
                            <td>${item.description}</td>
                            <td>${item.price}</td>
                            <td>
                                <form action="${pageContext.request.contextPath}/shop" method="post" style="float: left;">
                                    <input type="hidden" name="command" value="EDIT_ITEM_FORM"/>
                                    <input type="hidden" name="id" value="${item.id}"/>
                                    <button type="submit" class="btn-xs btn-warning"><fmt:message key="admin.items.button.edit"/></button>
                                </form>

                                <form action="${pageContext.request.contextPath}/shop" method="post">
                                    <input type="hidden" name="command" value="DELETE_ITEM"/>
                                    <input type="hidden" name="id" value="${item.id}"/>
                                    <button type="submit" class="btn-xs btn-danger"><fmt:message key="admin.items.button.delete"/></button>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
                <form action="${pageContext.request.contextPath}/shop" method="post">
                    <input type="hidden" name="command" value="HOME"/>
                    <button type="submit" class="btn btn-default">&lt;&lt; <fmt:message key="admin.items.back"/></button>
                </form>
            </div>
            <div class="col-md-4">
                <h5><fmt:message key="admin.items.pagenumber"/>: ${requestScope.page}</h5>
                <c:if test="${!empty requestScope.page and requestScope.page gt 0}">
                    <form action="${pageContext.request.contextPath}/shop" method="post" style="display: inline">
                        <input type="hidden" name="command" value="MANAGE_ITEMS"/>
                        <input type="hidden" name="page" value="${requestScope.page - 1}"/>
                        <input type="submit" value="&lt;&lt;" class="btn btn-default">
                    </form>
                </c:if>
                <c:if test="${!empty requestScope.page and requestScope.page lt requestScope.countPages - 1}">
                    <form action="${pageContext.request.contextPath}/shop" method="post" style="display: inline">
                        <input type="hidden" name="command" value="MANAGE_ITEMS"/>
                        <input type="hidden" name="page" value="${requestScope.page + 1}"/>
                        <input type="submit" value="&gt;&gt;" class="btn btn-default">
                    </form>
                </c:if>
                <br/>
                <form action="${pageContext.request.contextPath}/shop" method="post" style="display: inline">
                    <input type="hidden" name="command" value="NEW_ITEM_FORM"/>
                    <button type="submit" class="btn btn-success">+ <fmt:message key="admin.items.add.label"/></button>
                </form>
            </div>
        </div>
    </div>
</t:wrapper>