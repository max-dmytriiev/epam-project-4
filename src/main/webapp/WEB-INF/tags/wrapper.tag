<%@tag description="Page wrapper tag" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<!DOCTYPE html>
<html>
    <head>

        <!-- Bootstrap CSS -->
        <link
            rel="stylesheet"
            type="text/css"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>

        <!-- Bootstrap JS -->
        <script
            type="text/javascript"
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <!-- Bootstrap theme -->
        <link
            rel="stylesheet"
            type="text/css"
            href="https://bootswatch.com/paper/bootstrap.min.css"/>

        <title>
            Good Shop
        </title>

    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="${pageContext.request.contextPath}/index.jsp">Good Shop</a>
                </div>
                <c:choose>
                    <c:when test="${!empty sessionScope.user and sessionScope.user.permissionLevel == \"COMMON\"}">
                        <ul class="nav navbar-nav">
                            <li>
                                <form id="enterShopForm" action="${pageContext.request.contextPath}/shop" method="post" style="display:none;">
                                    <input type="hidden" name="command" value="LIST_ITEMS">
                                </form>
                                <a role="button" onclick="document.getElementById('enterShopForm').submit()"><fmt:message key="global.navbar.to-the-shop"/></a>
                            </li>
                            <li>
                                <form id="viewOrdersForm" action="${pageContext.request.contextPath}/shop" method="post" style="display:none;">
                                    <input type="hidden" name="command" value="LIST_ORDERS">
                                </form>
                                <a role="button" onclick="document.getElementById('viewOrdersForm').submit()"><fmt:message key="global.navbar.orders"/></a>
                            </li>
                        </ul>
                    </c:when>
                </c:choose>

                <ul class="nav navbar-nav navbar-right">
                    <c:choose>
                        <c:when test="${!empty sessionScope.user}">
                            <li>
                                <a href="${pageContext.request.contextPath}/user/dashboard.jsp"><fmt:message key="global.navbar.hello-label"/> ${sessionScope.user.login}</a>
                            </li>
                            <li>
                                <form id="logoutForm" action="${pageContext.request.contextPath}/shop" method="POST" style="display: none">
                                    <input type="hidden" name="command" value="LOGOUT"/>
                                </form>

                                <a onclick="document.getElementById('logoutForm').submit()" role="button">
                                    <span class="glyphicon glyphicon-log-out"></span>
                                    <span><fmt:message key="global.navbar.logout" /></span>
                                </a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li>
                                <a href="${pageContext.request.contextPath}/authentication/login.jsp">
                                    <span class="glyphicon glyphicon-log-in"></span>
                                    <span><fmt:message key="global.navbar.login" /></span>
                                </a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/authentication/register.jsp">
                                    <span class="glyphicon glyphicon-user"></span>
                                    <span><fmt:message key="global.navbar.register" /></span>
                                </a>
                            </li>
                        </c:otherwise>
                    </c:choose>
                </ul>
                <form class="nav navbar-form navbar-right">
                    <input type="hidden" name="command" value="LANGUAGE"/>
                    <div class="form-group">
                        <label for="language"><h6><fmt:message key="global.navbar.selector-label"/>:</h6></label>
                        <select id="language" name="language" class="form-control" onchange="submit()">
                            <option value="en_US" ${language == 'en_US' ? 'selected' : ''}>English</option>
                            <option value="ru_RU" ${language == 'ru_RU' ? 'selected' : ''}>Русский</option>
                        </select>
                    </div>
                </form>
            </div>
        </nav>

        <jsp:doBody/>
    </body>
</html>