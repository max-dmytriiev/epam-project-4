<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<t:wrapper>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h3><fmt:message key="auth.register.header"/></h3>
                <c:if test="${not empty requestScope.error}">
                    <div class="alert alert-danger">
                        <fmt:message key="auth.register.error.text"/>
                    </div>
                </c:if>
                <form action="${pageContext.request.contextPath}/shop" method="post">
                        <input type="hidden" name="command" value="REGISTER"/>
                    <div class="form-group">
                        <label for="first_name" class="lb-md">
                            <fmt:message key="auth.register.label.fname"/>:
                        </label>
                        <input
                                id="first_name"
                                type="text"
                                name="first_name"
                                required="required"
                                class="form-control"/>

                        <label for="last_name" class="lb-md">
                            <fmt:message key="auth.register.label.lname"/>:
                        </label>
                        <input
                                id="last_name"
                                type="text"
                                name="last_name"
                                required="required"
                                class="form-control"/>

                        <label for="login" class="lb-md">
                            <fmt:message key="auth.register.label.login"/>:
                        </label>
                        <input
                                id="login"
                                type="text"
                                name="login"
                                required="required"
                                class="form-control"/>

                        <label for="password" class="lb-md">
                            <fmt:message key="auth.register.label.password"/>:
                        </label>
                        <input
                                id="password"
                                type="password"
                                name="password"
                                required="required"
                                class="form-control"/>
                    </div>
                    <button type="submit" class="btn btn-primary"><fmt:message key="auth.register.label.submit"/></button>
                    <span>&nbsp;</span>
                    <a href="${pageContext.request.contextPath}/index.jsp">
                        <button type="submit" class="btn btn-default"><fmt:message key="auth.register.label.cancel"/></button>
                    </a>

                </form>
            </div>
        </div>
    </div>
</t:wrapper>