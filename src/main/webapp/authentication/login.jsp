<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<t:wrapper>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h3><fmt:message key="auth.login.header"/></h3>
                <form action="${pageContext.request.contextPath}/shop" method="post">
                    <input type="hidden" name="command" value="LOGIN"/>
                    <div class="form-group">
                        <label for="login" class="lb-md">
                            <fmt:message key="auth.login.label.login"/>:
                        </label>
                        <input
                                id="login"
                                type="text"
                                name="login"
                                required="required"
                                class="form-control"/>

                        <label for="password" class="lb-md">
                            <fmt:message key="auth.login.label.password"/>:
                        </label>
                        <input
                                id="password"
                                type="password"
                                name="password"
                                required="required"
                                class="form-control"/>
                    </div>
                    <button type="submit" class="btn btn-primary"><fmt:message key="auth.login.label.submit"/></button>
                    <span>&nbsp;</span>
                    <a href="${pageContext.request.contextPath}/index.jsp">
                        <button type="submit" class="btn btn-default"><fmt:message key="auth.login.label.cancel"/></button>
                    </a>

                </form>

            </div>
        </div>
    </div>
</t:wrapper>