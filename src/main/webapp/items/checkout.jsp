<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<t:wrapper>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <table class="table table-striped">
                    <tr>
                        <th><fmt:message key="admin.items.table.caption.image"/></th>
                        <th><fmt:message key="admin.items.table.caption.name"/></th>
                        <th><fmt:message key="admin.items.table.caption.description"/></th>
                        <th><fmt:message key="admin.items.table.caption.price"/></th>
                    </tr>
                    <c:forEach items="${requestScope.cart}" var="item">
                        <tr>
                            <td><img src="${item.image}" style="width: 30px"/></td>
                            <td>${item.name}</td>
                            <td>${item.description}</td>
                            <td>${item.price}</td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
            <div class="col-md-4">
                <h5><fmt:message key="user.items.checkout.total"/> ${requestScope.total}</h5>
                <hr/>

                <form action="${pageContext.request.contextPath}/shop" method="post">
                    <input type="hidden" name="command" value="PAY_ORDER"/>
                    <input type="hidden" name="total" value="${requestScope.total}"/>
                    <button type="submit" class="btn btn-default"><fmt:message key="user.items.checkout.pay"/></button>
                </form>

                <br/>
                <br/>
                <form action="${pageContext.request.contextPath}/shop" method="post">
                    <input type="hidden" name="command" value="LIST_ITEMS"/>
                    <button type="submit" class="btn btn-default">&lt;&lt; <fmt:message key="admin.items.back"/></button>
                </form>
            </div>
        </div>
    </div>
</t:wrapper>