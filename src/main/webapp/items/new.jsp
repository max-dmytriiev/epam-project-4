<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<t:wrapper>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                <form action="${pageContext.request.contextPath}/shop" method="post">
                    <input type="hidden" name="command" value="CREATE_ITEM"/>
                    <div class="form-group">
                        <label for="name" class="lb-md">
                            <fmt:message key="admin.items.new.label.name"/>:
                        </label>
                        <input
                                id="name"
                                type="text"
                                name="name"
                                required="required"
                                class="form-control"/>

                        <label for="description" class="lb-md">
                            <fmt:message key="admin.items.new.label.description"/>:
                        </label>
                        <input
                                id="description"
                                type="text"
                                name="description"
                                maxlength="200"
                                required="required"
                                class="form-control"/>

                        <label for="image" class="lb-md">
                            <fmt:message key="admin.items.new.label.image"/>:
                        </label>
                        <input
                                id="image"
                                type="text"
                                name="image"
                                required="required"
                                class="form-control"/>

                        <label for="price" class="lb-md">
                            <fmt:message key="admin.items.new.label.price"/>:
                        </label>
                        <input
                                id="price"
                                type="number"
                                name="price"
                                min="0"
                                step="0.01"
                                required="required"
                                class="form-control"/>
                    </div>
                    <button type="submit" class="btn btn-primary"><fmt:message key="admin.items.new.label.submit"/></button>
                </form>
                <form action="${pageContext.request.contextPath}/shop" method="post">
                    <input type="hidden" name="command" value="HOME"/>
                    <button type="submit" class="btn btn-default">&lt;&lt; <fmt:message key="admin.items.back"/></button>
                </form>
            </div>
        </div>
    </div>
</t:wrapper>