<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<t:wrapper>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <ul>
                    <c:forEach items="${requestScope.orders}" var="order">
                        <li>
                            <fmt:message key="orders.label.id" />(${order.id}): <fmt:message key="orders.label.value"/> ${order.value}
                        </li>
                    </c:forEach>
                </ul>
            </div>
            <div class="col-md-4">
                <h5><fmt:message key="admin.items.pagenumber"/>: ${requestScope.page}</h5>
                <c:if test="${!empty requestScope.page and requestScope.page gt 0}">
                    <form action="${pageContext.request.contextPath}/shop" method="post" style="display: inline">
                        <input type="hidden" name="command" value="LIST_ORDERS"/>
                        <input type="hidden" name="page" value="${requestScope.page - 1}"/>
                        <input type="submit" value="&lt;&lt;" class="btn btn-default">
                    </form>
                </c:if>
                <c:if test="${!empty requestScope.page and requestScope.page lt requestScope.countPages - 1}">
                    <form action="${pageContext.request.contextPath}/shop" method="post" style="display: inline">
                        <input type="hidden" name="command" value="LIST_ORDERS"/>
                        <input type="hidden" name="page" value="${requestScope.page + 1}"/>
                        <input type="submit" value="&gt;&gt;" class="btn btn-default">
                    </form>
                </c:if>

            </div>
        </div>
    </div>
</t:wrapper>