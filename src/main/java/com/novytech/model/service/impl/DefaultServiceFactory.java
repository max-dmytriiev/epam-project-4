package com.novytech.model.service.impl;

import com.novytech.model.service.ItemService;
import com.novytech.model.service.OrderService;
import com.novytech.model.service.ServiceFactory;
import com.novytech.model.service.UserService;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 16:59
 */
public class DefaultServiceFactory implements ServiceFactory {
    private static final DefaultServiceFactory instance = new DefaultServiceFactory();

    private DefaultServiceFactory() {}

    public static DefaultServiceFactory getInstance() {return instance;}

    @Override
    public UserService getUserService() {
        return DefaultUserService.getInstance();
    }

    @Override
    public ItemService getItemService() {
        return DefaultItemService.getInstance();
    }

    @Override
    public OrderService getOrderService() {
        return DefaultOrderService.getInstance();
    }
}
