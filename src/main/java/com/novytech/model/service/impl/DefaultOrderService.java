package com.novytech.model.service.impl;

import com.novytech.model.dao.OrderDao;
import com.novytech.model.dao.impl.JdbcDaoFactory;
import com.novytech.model.domain.Order;
import com.novytech.model.service.OrderService;
import com.novytech.model.service.ServiceFactory;

import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 11:50
 */
public class DefaultOrderService extends DefaultEntityService<Order> implements OrderService {
    private static final DefaultOrderService instance = new DefaultOrderService();

    private DefaultOrderService() {}

    public static DefaultOrderService getInstance() {return instance;}

    @Override
    OrderDao getDao() {
        return (OrderDao) JdbcDaoFactory.getInstance().getDaoForClass(Order.class);
    }

    @Override
    ServiceFactory getServiceFactory() {
        return DefaultServiceFactory.getInstance();
    }


    @Override
    public List<Order> findOrdersForUserWithPage(Integer user, Integer page) {
        return getDao().findAllByUserId(user, page);
    }

    @Override
    public Integer countByUserId(Integer user) {
        return getDao().countByUserId(user);
    }
}
