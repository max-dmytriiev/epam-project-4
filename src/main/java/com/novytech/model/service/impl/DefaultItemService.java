package com.novytech.model.service.impl;

import com.novytech.model.dao.ItemDao;
import com.novytech.model.dao.UserDao;
import com.novytech.model.dao.impl.JdbcDaoFactory;
import com.novytech.model.domain.Item;
import com.novytech.model.domain.PermissionLevel;
import com.novytech.model.domain.User;
import com.novytech.model.exceptions.LoginExistsException;
import com.novytech.model.exceptions.UniqueException;
import com.novytech.model.service.ItemService;
import com.novytech.model.service.ServiceFactory;
import com.novytech.model.service.UserService;

import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 16:56
 */
public class DefaultItemService extends DefaultEntityService<Item> implements ItemService {
    private static final DefaultItemService instance = new DefaultItemService();

    private DefaultItemService() {}

    public static DefaultItemService getInstance() {return instance;}

    @Override
    ItemDao getDao() {
        return (ItemDao) JdbcDaoFactory.getInstance().getDaoForClass(Item.class);
    }

    @Override
    ServiceFactory getServiceFactory() {
        return DefaultServiceFactory.getInstance();
    }

    @Override
    public List<Item> findByListOfIds(List<Integer> ids) {
        return getDao().findAll(ids);
    }
}
