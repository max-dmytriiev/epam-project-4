package com.novytech.model.service.impl;

import com.novytech.model.dao.GenericDao;
import com.novytech.model.domain.Entity;
import com.novytech.model.exceptions.UniqueException;
import com.novytech.model.service.EntityService;
import com.novytech.model.service.ServiceFactory;
import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 16:18
 */
abstract class DefaultEntityService<T extends Entity> implements EntityService<T> {

    abstract GenericDao<T> getDao();
    abstract ServiceFactory getServiceFactory();

    @Override
    public boolean create(T t) throws UniqueException {
        return getDao().create(t);
    }

    @Override
    public boolean update(T t) {
        return getDao().update(t);
    }

    @Override
    public boolean delete(int id) {
        return getDao().delete(id);
    }

    @Override
    public T find(int id) {
        return getDao().find(id);
    }

    @Override
    public List<T> findAll() {
        return getDao().findAll();
    }

    @Override
    public List<T> findAll(int page) {
        return getDao().findAll(page);
    }

    @Override
    public Integer count() {
        return getDao().count();
    }
}
