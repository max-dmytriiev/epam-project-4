package com.novytech.model.service.impl;

import com.novytech.model.dao.UserDao;
import com.novytech.model.dao.impl.JdbcDaoFactory;
import com.novytech.model.domain.PermissionLevel;
import com.novytech.model.domain.User;
import com.novytech.model.exceptions.LoginExistsException;
import com.novytech.model.exceptions.UniqueException;
import com.novytech.model.service.ServiceFactory;
import com.novytech.model.service.UserService;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 16:56
 */
public class DefaultUserService extends DefaultEntityService<User> implements UserService {
    private static final DefaultUserService instance = new DefaultUserService();

    private DefaultUserService() {}

    public static DefaultUserService getInstance() {return instance;}

    @Override
    UserDao getDao() {
        return (UserDao) JdbcDaoFactory.getInstance().getDaoForClass(User.class);
    }

    @Override
    ServiceFactory getServiceFactory() {
        return DefaultServiceFactory.getInstance();
    }

    @Override
    public boolean create(User user) throws UniqueException {
        if (getDao().getByLogin(user.getLogin()) != null) {
            throw new LoginExistsException();
        }
        return super.create(user);
    }

    @Override
    public User loginUser(String login, String password) {
        User user = getDao().getByLogin(login);
        if (user != null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }

    @Override
    public void block(int id) {
        User user = find(id);
        user.setPermissionLevel(PermissionLevel.BLACKLIST);
        update(user);
    }

    @Override
    public void unblock(int id) {
        User user = find(id);
        user.setPermissionLevel(PermissionLevel.COMMON);
        update(user);
    }

    @Override
    public User findByLogin(String login) {
        return getDao().getByLogin(login);
    }
}
