package com.novytech.model.service;

import com.novytech.model.domain.Order;

import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 11:50
 */
public interface OrderService extends EntityService<Order> {
    List<Order> findOrdersForUserWithPage(Integer user, Integer page);
    Integer countByUserId(Integer user);
}
