package com.novytech.model.service;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 16:20
 */
public interface ServiceFactory {
    UserService getUserService();
    ItemService getItemService();
    OrderService getOrderService();
}
