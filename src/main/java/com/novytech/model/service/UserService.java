package com.novytech.model.service;

import com.novytech.model.domain.User;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 16:56
 */
public interface UserService extends EntityService<User> {
    User loginUser(String login, String password);

    void block(int id);
    void unblock(int id);

    User findByLogin(String login);
}
