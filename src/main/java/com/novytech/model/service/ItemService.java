package com.novytech.model.service;

import com.novytech.model.domain.Item;

import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 0:43
 */
public interface ItemService extends EntityService<Item> {
    List<Item> findByListOfIds(List<Integer> ids);
}
