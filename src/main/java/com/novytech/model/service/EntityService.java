package com.novytech.model.service;

import com.novytech.model.domain.Entity;
import com.novytech.model.domain.User;
import com.novytech.model.exceptions.UniqueException;

import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 16:10
 */
public interface EntityService<T extends Entity> {

    boolean create(T t) throws UniqueException;

    boolean update(T t);

    boolean delete(int id);

    T find(int id);

    List<T> findAll();

    List<T> findAll(int page);

    Integer count();
}
