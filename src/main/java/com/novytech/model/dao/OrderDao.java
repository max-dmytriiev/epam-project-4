package com.novytech.model.dao;

import com.novytech.model.domain.Order;

import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 11:35
 */
public interface OrderDao extends GenericDao<Order> {
    List<Order> findAllByUserId(Integer user, Integer page);
    Integer countByUserId(Integer user);
}
