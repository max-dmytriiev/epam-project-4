package com.novytech.model.dao;

import com.novytech.model.domain.Item;

import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 0:13
 */
public interface ItemDao extends GenericDao<Item> {
    List<Item> findAll(List<Integer> ids);
}
