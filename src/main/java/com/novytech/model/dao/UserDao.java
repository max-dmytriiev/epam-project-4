package com.novytech.model.dao;

import com.novytech.model.domain.User;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 14:26
 */
public interface UserDao extends GenericDao<User> {
    User getByLogin(String login);
}
