package com.novytech.model.dao;
import com.novytech.model.domain.Entity;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 04.12.16, 14:10
 */
public interface GenericDao<T extends Entity> {

    /**
     * Create the corresponding table if not exists
     */
    void createTableIfNotExists();

    /**
     * Persist a new entity in the database
     * @param newInstance an instance to persist in database
     * @return value of Primary Key from created instance
     */
    boolean create(T newInstance);

    /**
     * Retrieve an instance from database by id
     * @param id primary key
     * @return instance of type T which was stored in the database with the specified primary key
     */
    T find(Integer id);

    /**
     * Update an instance in the database
     * @param instance an instance to update
     */
    boolean update(T instance);

    /**
     * Remove an object from database
     * @param id id of object to remove
     */
    boolean delete(Integer id);

    /**
     * @return List of all entities
     */
    List<T> findAll();

    /**
     * @return List of all entities on page
     */
    List<T> findAll(int page);

    /**
     * @return Count of objects
     */
    Integer count();
}
