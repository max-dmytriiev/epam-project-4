package com.novytech.model.dao.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Maksym Dmytriiev
 * @since 16.12.16, 16:35
 */
public class QueryHelper {
    private static QueryHelper instance;
    public static synchronized QueryHelper getInstance() {
        if (instance == null) {
            Properties properties = null;
            try {
                properties = new Properties();
                InputStream is = QueryHelper.class.getClassLoader().getResourceAsStream("/querydata.properties");
                properties.load(is);

            } catch (IOException e) {
                e.printStackTrace();
            }

            instance = new QueryHelper(properties);
        }
        return instance;
    }

    Properties queryData;

    private QueryHelper(Properties queryData) {
        this.queryData = queryData;
    }

    public String getRequestStringForKey(String key) {
        return queryData.getProperty(key);
    }
}
