package com.novytech.model.dao.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Maksym Dmytriiev
 * @since 16.12.16, 15:43
 */
public class PasswordHelper {
    private static PasswordHelper instance = new PasswordHelper();

    private Logger logger = LogManager.getLogger(PasswordHelper.class);

    private PasswordHelper(){
        // Exists only to prevent instantiation
    }

    public static PasswordHelper getInstance() {return instance;}

    public String hash(String data) {
        MessageDigest md;
        byte[] byteData;
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(data.getBytes());
            byteData = md.digest();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            logger.error(e);
            return null;
        }
    }
}
