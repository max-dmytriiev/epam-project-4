package com.novytech.model.dao;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 16:52
 */
public interface DaoFactory {
    GenericDao getDaoForClass(Class c);
}
