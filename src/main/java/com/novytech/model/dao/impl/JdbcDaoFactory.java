package com.novytech.model.dao.impl;

import com.novytech.model.dao.DaoFactory;
import com.novytech.model.dao.GenericDao;
import com.novytech.model.domain.Item;
import com.novytech.model.domain.Order;
import com.novytech.model.domain.User;

/**
 * @author Maksym Dmytriiev
 * @since 08.12.16, 13:51
 */
public class JdbcDaoFactory implements DaoFactory {
    private static JdbcDaoFactory instance = new JdbcDaoFactory();

    private JdbcDaoFactory() {
        // Exists only to prevent instantiation
    }

    public static JdbcDaoFactory getInstance() {return instance;}

    @Override
    public GenericDao getDaoForClass(Class c) {
        if (c == User.class) {
            return JdbcUserDao.getInstance();
        }
        if (c == Item.class) {
            return JdbcItemDao.getInstance();
        }
        if (c == Order.class) {
            return JdbcOrderDao.getInstance();
        }
        throw new IllegalArgumentException("Invalid class parameter");
    }
}
