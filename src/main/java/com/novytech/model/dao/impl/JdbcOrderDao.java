package com.novytech.model.dao.impl;

import com.novytech.model.dao.OrderDao;
import com.novytech.model.dao.connection.impl.JdbcConnectionFactory;
import com.novytech.model.dao.util.QueryHelper;
import com.novytech.model.domain.Order;
import com.novytech.model.domain.OrderFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 11:36
 */
public class JdbcOrderDao implements OrderDao {

    private static JdbcOrderDao instance;
    private final Logger logger = LogManager.getLogger(JdbcOrderDao.class);

    private Connection connection;

    private JdbcOrderDao(Connection connection) {
        this.connection = connection;
    }

    public static synchronized JdbcOrderDao getInstance(){
        if (instance == null) {
            instance = new JdbcOrderDao(JdbcConnectionFactory.getInstance().getConnection());
        }
        return instance;
    }

    @Override
    public void createTableIfNotExists() {

    }

    @Override
    public boolean create(Order newInstance) {
        boolean wasCreated = false;

        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("orders.create");

            PreparedStatement ps = this.connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            ps.setInt(1, newInstance.getUser());
            ps.setFloat(2, newInstance.getValue());

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating item failed, no rows affected.");
            }

            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            newInstance.setId(rs.getInt(1));

            wasCreated = true;
        } catch (SQLException e) {
            logger.error(e);
        }

        return wasCreated;
    }

    @Override
    public Order find(Integer id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean update(Order instance) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(Integer id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Order> findAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Order> findAll(int page) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Integer count() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Order> findAllByUserId(Integer user, Integer page) {
        List<Order> orders = new ArrayList<>();
        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("orders.read.by.user.with.page");

            PreparedStatement ps = this.connection.prepareStatement(query);
            ps.setInt(1, user);

            int limit = 10;
            int offset = page * limit;

            ps.setInt(2, limit);
            ps.setInt(3, offset);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Integer id = rs.getInt("id");
                Float value = rs.getFloat("total");

                Order order = OrderFactory.getInstance().createOrderWithId(id, user, value);

                orders.add(order);
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return orders;
    }

    @Override
    public Integer countByUserId(Integer user) {
        Integer count = -1;
        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("orders.count.by.user");

            PreparedStatement ps = this.connection.prepareStatement(query);
            ps.setInt(1, user);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                count = rs.getInt("total");
            }
        } catch (SQLException e) {
            logger.error(e);
        }

        return count;
    }
}
