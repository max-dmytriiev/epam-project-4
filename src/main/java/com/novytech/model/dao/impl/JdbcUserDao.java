package com.novytech.model.dao.impl;

import com.novytech.model.dao.UserDao;
import com.novytech.model.dao.connection.impl.JdbcConnectionFactory;
import com.novytech.model.dao.util.QueryHelper;
import com.novytech.model.domain.PermissionLevel;
import com.novytech.model.domain.User;
import com.novytech.model.domain.UserFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 04.12.16, 16:27
 */
public class JdbcUserDao implements UserDao {

    private static JdbcUserDao instance;
    private final Logger logger = LogManager.getLogger(JdbcUserDao.class);

    private Connection connection;


    private JdbcUserDao(Connection connection) {
        this.connection = connection;
    }

    public static synchronized JdbcUserDao getInstance(){
        if (instance == null) {
            instance = new JdbcUserDao(JdbcConnectionFactory.getInstance().getConnection());
        }
        return instance;
    }

    @Override
    public void createTableIfNotExists() {
        /*TODO */
    }

    @Override
    public boolean create(User newInstance) {
        boolean wasCreated = false;

        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("users.create");

            PreparedStatement ps = this.connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, newInstance.getFirstName());
            ps.setString(2, newInstance.getLastName());
            ps.setString(3, newInstance.getLogin());
            ps.setString(4, newInstance.getPassword());
            ps.setString(5, newInstance.getPermissionLevel().name());

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }

            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            newInstance.setId(rs.getInt(1));

            wasCreated = true;
        } catch (SQLException e) {
            logger.error(e);
        }

        return wasCreated;
    }

    @Override
    public User find(Integer id) {
        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("users.read.by.id");

            PreparedStatement ps = this.connection.prepareStatement(query);

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                String login = rs.getString("login");
                String password = rs.getString("password_hash");
                PermissionLevel permissionLevel = PermissionLevel.valueOf(rs.getString("permission_level"));

                return UserFactory.getInstance().createUserWithId(id, firstName, lastName, login, password, permissionLevel);
            }
        } catch (SQLException e) {
            logger.error(e);
        }

        return null;
    }

    @Override
    public boolean update(User instance){
        boolean wasUpdated = false;
        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("users.update");
            PreparedStatement ps = this.connection.prepareStatement(query);

            ps.setString(1, instance.getFirstName());
            ps.setString(2, instance.getLastName());
            ps.setString(3, instance.getLogin());
            ps.setString(4, instance.getPassword());
            ps.setString(5, instance.getPermissionLevel().name());
            ps.setInt(6, instance.getId());

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Update failed, no rows affected.");
            }

            wasUpdated = true;
        } catch (SQLException e) {
            logger.error(e);
        }

        return wasUpdated;
    }

    @Override
    public boolean delete(Integer id) {
        boolean wasDeleted = false;
        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("users.delete");
            PreparedStatement ps = this.connection.prepareStatement(query);

            ps.setInt(1, id);

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Delete failed, no rows affected.");
            }

            wasDeleted = true;
        } catch (SQLException e) {
            logger.error(e);
        }

        return wasDeleted;
    }

    @Override
    public List<User> findAll() {
        List<User> allUsers = new ArrayList<>();
        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("users.read.all");
            PreparedStatement ps = this.connection.prepareStatement(query);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Integer id = rs.getInt("id");
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                String login = rs.getString("login");
                String password = rs.getString("password_hash");
                PermissionLevel permissionLevel = PermissionLevel.valueOf(rs.getString("permission_level"));

                User newUser = UserFactory.getInstance().createUserWithId(id, firstName, lastName, login, password, permissionLevel);
                allUsers.add(newUser);
            }
        } catch (SQLException e) {
            logger.error(e);
        }

        return allUsers;
    }

    @Override
    public List<User> findAll(int page) {
        List<User> allUsers = new ArrayList<>();
        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("users.read.all.page");
            PreparedStatement ps = this.connection.prepareStatement(query);

            int limit = 10;
            int offset = page * limit;

            ps.setInt(1, limit);
            ps.setInt(2, offset);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Integer id = rs.getInt("id");
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                String login = rs.getString("login");
                String password = rs.getString("password_hash");
                PermissionLevel permissionLevel = PermissionLevel.valueOf(rs.getString("permission_level"));

                User newUser = UserFactory.getInstance().createUserWithId(id, firstName, lastName, login, password, permissionLevel);
                allUsers.add(newUser);
            }
        } catch (SQLException e) {
            logger.error(e);
        }

        return allUsers;
    }

    @Override
    public Integer count() {
        Integer count = -1;
        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("users.count");

            PreparedStatement ps = this.connection.prepareStatement(query);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                count = rs.getInt("total");
            }
        } catch (SQLException e) {
            logger.error(e);
        }

        return count;
    }

    @Override
    public User getByLogin(String login) {
        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("users.read.by.login");

            PreparedStatement ps = this.connection.prepareStatement(query);

            ps.setString(1, login);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                Integer id = rs.getInt("id");
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                String password = rs.getString("password_hash");
                PermissionLevel permissionLevel = PermissionLevel.valueOf(rs.getString("permission_level"));

                return UserFactory.getInstance().createUserWithId(id, firstName, lastName, login, password, permissionLevel);
            }
        } catch (SQLException e) {
            logger.error(e);
        }

        return null;
    }

}
