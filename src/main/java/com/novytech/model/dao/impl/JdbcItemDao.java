package com.novytech.model.dao.impl;

import com.novytech.model.dao.ItemDao;
import com.novytech.model.dao.connection.impl.JdbcConnectionFactory;
import com.novytech.model.dao.util.QueryHelper;
import com.novytech.model.domain.Item;
import com.novytech.model.domain.ItemFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 0:13
 */
public class JdbcItemDao implements ItemDao {

    private static JdbcItemDao instance;
    private final Logger logger = LogManager.getLogger(JdbcItemDao.class);

    private Connection connection;

    private JdbcItemDao(Connection connection) {
        this.connection = connection;
    }

    public static synchronized JdbcItemDao getInstance(){
        if (instance == null) {
            instance = new JdbcItemDao(JdbcConnectionFactory.getInstance().getConnection());
        }
        return instance;
    }

    @Override
    public void createTableIfNotExists() {
        /*TODO*/
    }

    @Override
    public boolean create(Item newInstance) {
        boolean wasCreated = false;

        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("goods.create");

            PreparedStatement ps = this.connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, newInstance.getName());
            ps.setString(2, newInstance.getDescription());
            ps.setString(3, newInstance.getImage());
            ps.setFloat(4, newInstance.getPrice());

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating item failed, no rows affected.");
            }

            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            newInstance.setId(rs.getInt(1));

            wasCreated = true;
        } catch (SQLException e) {
            logger.error(e);
        }

        return wasCreated;
    }

    @Override
    public Item find(Integer id) {
        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("goods.read.by.id");

            PreparedStatement ps = this.connection.prepareStatement(query);

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                String name = rs.getString("name");
                String description = rs.getString("description");
                String image = rs.getString("image");
                Float price = rs.getFloat("price");

                return ItemFactory.getInstance().createItemWithId(id, name, description, image, price);
            }
        } catch (SQLException e) {
            logger.error(e);
        }

        return null;
    }

    @Override
    public boolean update(Item instance) {
        boolean wasUpdated = false;
        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("goods.update");
            PreparedStatement ps = this.connection.prepareStatement(query);

            ps.setString(1, instance.getName());
            ps.setString(2, instance.getDescription());
            ps.setString(3, instance.getImage());
            ps.setFloat(4, instance.getPrice());
            ps.setInt(5, instance.getId());

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Update failed, no rows affected.");
            }

            wasUpdated = true;
        } catch (SQLException e) {
            logger.error(e);
        }

        return wasUpdated;
    }

    @Override
    public boolean delete(Integer id) {
        boolean wasDeleted = false;
        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("goods.delete");
            PreparedStatement ps = this.connection.prepareStatement(query);

            ps.setInt(1, id);

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Delete failed, no rows affected.");
            }

            wasDeleted = true;
        } catch (SQLException e) {
            logger.error(e);
        }

        return wasDeleted;
    }

    @Override
    public List<Item> findAll() {
        List<Item> allItems = new ArrayList<>();
        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("goods.read.all");
            PreparedStatement ps = this.connection.prepareStatement(query);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Integer id = rs.getInt("id");
                String name = rs.getString("name");
                String description = rs.getString("description");
                String image = rs.getString("image");
                Float price = rs.getFloat("price");

                Item item = ItemFactory.getInstance().createItemWithId(id, name, description, image, price);
                allItems.add(item);
            }
        } catch (SQLException e) {
            logger.error(e);
        }

        return allItems;
    }

    @Override
    public List<Item> findAll(int page) {
        List<Item> allItems = new ArrayList<>();
        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("goods.read.all.page");
            PreparedStatement ps = this.connection.prepareStatement(query);

            int limit = 10;
            int offset = page * limit;

            ps.setInt(1, limit);
            ps.setInt(2, offset);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Integer id = rs.getInt("id");
                String name = rs.getString("name");
                String description = rs.getString("description");
                String image = rs.getString("image");
                Float price = rs.getFloat("price");

                Item item = ItemFactory.getInstance().createItemWithId(id, name, description, image, price);
                allItems.add(item);
            }
        } catch (SQLException e) {
            logger.error(e);
        }

        return allItems;
    }

    @Override
    public Integer count() {
        Integer count = -1;
        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("goods.count");

            PreparedStatement ps = this.connection.prepareStatement(query);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                count = rs.getInt("total");
            }
        } catch (SQLException e) {
            logger.error(e);
        }

        return count;
    }

    @Override
    public List<Item> findAll(List<Integer> ids) {
        List<Item> listedItems = new ArrayList<>();
        try {
            String query = QueryHelper.getInstance().getRequestStringForKey("goods.read.by.id.in.list");
            PreparedStatement ps = this.connection.prepareStatement(query);

            ps.setArray(1, connection.createArrayOf("int4", ids.toArray()));
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Integer id = rs.getInt("id");
                String name = rs.getString("name");
                String description = rs.getString("description");
                String image = rs.getString("image");
                Float price = rs.getFloat("price");

                Item item = ItemFactory.getInstance().createItemWithId(id, name, description, image, price);
                listedItems.add(item);
            }
        } catch (SQLException e) {
            logger.error(e);
        }

        return listedItems;
    }
}
