package com.novytech.model.dao.connection.impl;

import com.novytech.model.dao.connection.ConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Maksym Dmytriiev
 * @since 04.12.16, 14:39
 */
public class JdbcConnectionFactory implements ConnectionFactory{

    private static JdbcConnectionFactory instance;
    private DataSource dataSource;

    private static final Logger logger = LogManager.getLogger(JdbcConnectionFactory.class);

    private JdbcConnectionFactory(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Basic implementation of a singleton pattern
     * @return Instance of a connection provider
     */
    public static synchronized JdbcConnectionFactory getInstance(){
        if (instance == null) {

            /* Perform connection provider initialization here */
            Context initCtx = null;
            try {
                initCtx = new InitialContext();
                Context envCtx = (Context) initCtx.lookup("java:comp/env");
                DataSource dataSource = (DataSource) envCtx.lookup("jdbc/shop");

                instance = new JdbcConnectionFactory(dataSource);

            } catch (NamingException e) {
                logger.error(e);
            }
        }
        return instance;
    }

    @Override
    public Connection getConnection() {
        Connection c = null;
        try {
            c = dataSource.getConnection();
        } catch (SQLException e) {
            logger.error(e);
        }

        return c;
    }
}
