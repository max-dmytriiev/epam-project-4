package com.novytech.model.dao.connection;

import java.sql.Connection;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 14:36
 */
public interface ConnectionFactory {
    Connection getConnection();
}