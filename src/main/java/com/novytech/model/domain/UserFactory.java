package com.novytech.model.domain;

/**
 * @author Maksym Dmytriiev
 * @since 04.12.16, 13:41
 */
public class UserFactory {
    private static UserFactory instance = new UserFactory();

    /**
     * Basic singleton pattern implementation
     * @return Instance of User Factory class
     */
    public static UserFactory getInstance() {return instance;}

    private UserFactory() {
        // Exists only to prevent instantiation
    }

    public User createUserWithId(
            Integer id,
            String firstName,
            String lastName,
            String login,
            String passwordHash,
            PermissionLevel permissionLevel) {

        return new User(id, firstName, lastName, login, passwordHash, permissionLevel);
    }

    public User createUserWithoutId(
            String firstName,
            String lastName,
            String login,
            String passwordHash,
            PermissionLevel permissionLevel) {

        return createUserWithId(null, firstName, lastName, login, passwordHash, permissionLevel);
    }
}
