package com.novytech.model.domain;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 11:28
 */
public class Order implements Entity{
    private Integer id;
    private Integer user;
    private Float value;

    Order(Integer id, Integer user, Float value) {
        this.id = id;
        this.user = user;
        this.value = value;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }
}
