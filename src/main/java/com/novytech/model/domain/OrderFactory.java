package com.novytech.model.domain;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 11:29
 */
public class OrderFactory {
    private static OrderFactory instance = new OrderFactory();

    /**
     * Basic singleton pattern implementation
     * @return Instance of Order Factory class
     */
    public static OrderFactory getInstance() {return instance;}

    private OrderFactory() {
        // Exists only to prevent instantiation
    }

    public Order createOrderWithId(
            Integer id,
            Integer user,
            Float value) {


        return new Order(id, user, value);
    }

    public Order createOrderWithoutId(
            Integer user,
            Float value) {

        return new Order(null, user, value);
    }
}
