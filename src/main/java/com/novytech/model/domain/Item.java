package com.novytech.model.domain;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 0:09
 */
public class Item implements Entity {

    private Integer id;
    private String name;
    private String description;
    private String image;
    private Float price;

    Item(Integer id, String name, String description, String image, Float price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.price = price;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}
