package com.novytech.model.domain;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 0:10
 */
public class ItemFactory {
    private static ItemFactory instance = new ItemFactory();

    /**
     * Basic singleton pattern implementation
     * @return Instance of Item Factory class
     */
    public static ItemFactory getInstance() {return instance;}

    private ItemFactory() {
        // Exists only to prevent instantiation
    }

    public Item createItemWithId(
            Integer id,
            String name,
            String description,
            String image,
            Float price) {


        return new Item(id, name, description, image, price);
    }

    public Item createItemWithoutId(
            String name,
            String description,
            String image,
            Float price) {


        return new Item(null, name, description, image, price);
    }

}
