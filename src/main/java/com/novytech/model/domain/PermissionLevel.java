package com.novytech.model.domain;

/**
 * @author Maksym Dmytriiev
 * @since 04.12.16, 12:48
 */
public enum PermissionLevel {
    BLACKLIST,
    GUEST,
    COMMON,
    ADMIN
}
