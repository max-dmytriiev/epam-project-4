package com.novytech.model.domain;

import java.io.Serializable;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 14:12
 */
public interface Entity extends Serializable {
    Integer getId();
    void setId(Integer id);
}
