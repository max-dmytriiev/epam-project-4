package com.novytech.model.exceptions;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 17:15
 */
public class LoginExistsException extends UniqueException {
}
