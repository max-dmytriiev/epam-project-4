package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.model.domain.Item;
import com.novytech.model.domain.ItemFactory;
import com.novytech.model.domain.PermissionLevel;
import com.novytech.model.domain.User;
import com.novytech.model.service.ItemService;
import com.novytech.model.service.impl.DefaultServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 9:54
 */
public class ItemsEditCommand implements Command {
    private static final ItemsEditCommand instance = new ItemsEditCommand();
    private final Logger logger = LogManager.getLogger(ItemsEditCommand.class);

    private final ItemService itemService = DefaultServiceFactory.getInstance().getItemService();

    private ItemsEditCommand() {}

    static ItemsEditCommand getInstance() {return instance;}

    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {
        if (user == null || user.getPermissionLevel() != PermissionLevel.ADMIN) {
            throw new InsufficientPermissionsException();
        }

        Integer id = Integer.parseInt(req.getParameter("id"));
        String name = req.getParameter("name");
        String description = req.getParameter("description");
        String image = req.getParameter("image");
        Float price = Float.parseFloat(req.getParameter("price"));

        Item item = ItemFactory.getInstance().createItemWithId(id, name, description, image, price);

        itemService.update(item);

        return ManageItemsCommand.getInstance().execute(req, user);
    }
}
