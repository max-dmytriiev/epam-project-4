package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.model.domain.Order;
import com.novytech.model.domain.OrderFactory;
import com.novytech.model.domain.User;
import com.novytech.model.exceptions.UniqueException;
import com.novytech.model.service.impl.DefaultServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 11:18
 */
public class PayOrderCommand implements Command {
    private static final PayOrderCommand instance = new PayOrderCommand();
    private final Logger logger = LogManager.getLogger();
    private PayOrderCommand(){}

    public static PayOrderCommand getInstance() {return instance;}


    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {
        Float total = Float.parseFloat(req.getParameter("total"));
        User buyer = req.getSessionWrapper(false).getUser();

        Order order = OrderFactory.getInstance().createOrderWithoutId(buyer.getId(), total);
        try {
            DefaultServiceFactory.getInstance().getOrderService().create(order);
        } catch (UniqueException e) {
            logger.error(e);
        }
        req.getSessionWrapper(false).emptyCart();

        return UserHomePageCommand.getInstance().execute(req, user);
    }
}
