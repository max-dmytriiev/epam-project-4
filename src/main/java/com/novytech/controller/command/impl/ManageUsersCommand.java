package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.JspViews;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.model.domain.PermissionLevel;
import com.novytech.model.domain.User;
import com.novytech.model.service.impl.DefaultServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 22:06
 */
public class ManageUsersCommand implements Command {
    private static final ManageUsersCommand instance = new ManageUsersCommand();
    private final Logger logger = LogManager.getLogger(ManageUsersCommand.class);

    private ManageUsersCommand() {}

    public static ManageUsersCommand getInstance() {return instance;}

    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {
        if (user == null || user.getPermissionLevel() != PermissionLevel.ADMIN) {
            throw new InsufficientPermissionsException();
        }

        int countPages = 0;
        int countItems = 0;
        String countStr = req.getParameter("countPages");
        if (countStr == null) {
            countItems = DefaultServiceFactory.getInstance().getUserService().count();
            int div = countItems / 10;
            int mod = countItems % 10;
            countPages = div;
            if (mod > 0) {
                ++countPages;
            }
        } else {
            countPages = Integer.parseInt(countStr);
        }

        String pageStr = req.getParameter("page");
        int page = 0;
        if (pageStr != null) {
            page = Integer.parseInt(pageStr);
        }

        List<User> users = DefaultServiceFactory.getInstance().getUserService().findAll(page);

        req.setAttribute("users", users);
        req.setAttribute("page", page);
        req.setAttribute("countPages", countPages);
        return JspViews.ADMIN.MANAGE_USERS_PAGE;
    }
}
