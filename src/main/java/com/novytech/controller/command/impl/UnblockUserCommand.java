package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.model.domain.PermissionLevel;
import com.novytech.model.domain.User;
import com.novytech.model.service.ServiceFactory;
import com.novytech.model.service.UserService;
import com.novytech.model.service.impl.DefaultServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 23:46
 */
public class UnblockUserCommand implements Command {
    private static final UnblockUserCommand instance = new UnblockUserCommand();
    private final Logger logger = LogManager.getLogger(UnblockUserCommand.class);
    private final ServiceFactory sf = DefaultServiceFactory.getInstance();
    private UnblockUserCommand(){}

    public static UnblockUserCommand getInstance() {return instance;}


    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {

        if (user == null || user.getPermissionLevel() != PermissionLevel.ADMIN) {
            throw new InsufficientPermissionsException();
        }

        UserService userService = sf.getUserService();

        String idStr = req.getParameter("id");
        Integer id = Integer.parseInt(idStr);

        userService.unblock(id);
        logger.info("User " + id + " has been un-blocked");

        return ManageUsersCommand.getInstance().execute(req, user);
    }
}
