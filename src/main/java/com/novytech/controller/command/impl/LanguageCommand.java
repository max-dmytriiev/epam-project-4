package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.JspViews;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.controller.util.SessionWrapper;
import com.novytech.model.domain.User;

import javax.swing.text.View;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 20:42
 */
public class LanguageCommand implements Command {
    private static final LanguageCommand instance = new LanguageCommand();

    private LanguageCommand() {}

    public static LanguageCommand getInstance() {return instance;}

    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {
        SessionWrapper sw = req.getSessionWrapper(true);
        String language = req.getParameter("language");
        sw.setLocale(language);
        if (user == null) {
            return JspViews.GENERAL.INDEX_PAGE;
        }
        return UserHomePageCommand.getInstance().execute(req, user);
    }
}
