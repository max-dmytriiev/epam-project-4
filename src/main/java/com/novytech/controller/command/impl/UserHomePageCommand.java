package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.JspViews;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.model.domain.PermissionLevel;
import com.novytech.model.domain.User;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 17:49
 */
public class UserHomePageCommand implements Command {
    private static UserHomePageCommand instance = new UserHomePageCommand();

    private UserHomePageCommand(){}

    public static UserHomePageCommand getInstance() {return instance;}

    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {
        if (user == null) {
            return JspViews.GENERAL.INDEX_PAGE;
        }

        if (user.getPermissionLevel() == PermissionLevel.BLACKLIST) {
            req.getSessionWrapper(false).invalidate();
            return JspViews.GENERAL.BLOCKED_PAGE;
        }

        if (user.getPermissionLevel() == PermissionLevel.ADMIN) {
            return JspViews.ADMIN.INDEX_PAGE;
        }

        return ItemsListCommand.getInstance().execute(req, user);
    }
}
