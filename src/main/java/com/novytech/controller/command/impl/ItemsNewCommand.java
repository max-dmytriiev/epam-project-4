package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.exceptions.InvalidParameterValueException;
import com.novytech.controller.util.JspViews;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.controller.util.SessionWrapper;
import com.novytech.model.dao.util.PasswordHelper;
import com.novytech.model.domain.*;
import com.novytech.model.exceptions.UniqueException;
import com.novytech.model.service.ItemService;
import com.novytech.model.service.UserService;
import com.novytech.model.service.impl.DefaultServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 15:53
 */
public class ItemsNewCommand implements Command {
    private static final ItemsNewCommand instance = new ItemsNewCommand();
    private final Logger logger = LogManager.getLogger(ItemsNewCommand.class);

    private final ItemService itemService = DefaultServiceFactory.getInstance().getItemService();

    private ItemsNewCommand() {}

    static Command getInstance() {return instance;}

    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {
        if (user == null || user.getPermissionLevel() != PermissionLevel.ADMIN) {
            throw new InsufficientPermissionsException();
        }

        try {
            String name = req.getParameter("name");
            String description = req.getParameter("description");
            String image = req.getParameter("image");
            Float price = Float.parseFloat(req.getParameter("price"));

            Item item = ItemFactory.getInstance().createItemWithoutId(name, description, image, price);

            boolean created = itemService.create(item);

            if (created) {
                return ManageItemsCommand.getInstance().execute(req, user);
            }

        } catch (UniqueException e) {
            logger.error(e);
        }

        return ItemsNewFormCommand.getInstance().execute(req, user);
    }
}
