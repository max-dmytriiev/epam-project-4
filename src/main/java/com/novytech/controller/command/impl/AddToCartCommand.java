package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.controller.util.SessionWrapper;
import com.novytech.model.domain.User;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 10:25
 */
public class AddToCartCommand implements Command {
    private static final AddToCartCommand instance = new AddToCartCommand();
    private AddToCartCommand(){}
    public static AddToCartCommand getInstance() {return instance;}

    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {
        SessionWrapper sw = req.getSessionWrapper(false);

        Integer id = Integer.parseInt(req.getParameter("id"));

        Integer page = Integer.parseInt(req.getParameter("page"));
        Integer countPages = Integer.parseInt(req.getParameter("countPages"));

        sw.addToCart(id);

        req.setAttribute("page", page);
        req.setAttribute("countPages", countPages);

        return ItemsListCommand.getInstance().execute(req, user);
    }
}
