package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.JspViews;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.model.domain.PermissionLevel;
import com.novytech.model.domain.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 9:03
 */
public class ItemsNewFormCommand implements Command {
    private static final ItemsNewFormCommand instance = new ItemsNewFormCommand();
    private final Logger logger = LogManager.getLogger(ItemsNewFormCommand.class);
    private ItemsNewFormCommand(){}

    public static ItemsNewFormCommand getInstance() {return instance;}


    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {
        if (user == null || user.getPermissionLevel() != PermissionLevel.ADMIN) {
            throw new InsufficientPermissionsException();
        }

        return JspViews.ITEMS.CREATE_FORM;
    }
}