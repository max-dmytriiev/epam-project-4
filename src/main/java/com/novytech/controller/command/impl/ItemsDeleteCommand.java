package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.JspViews;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.model.domain.Item;
import com.novytech.model.domain.PermissionLevel;
import com.novytech.model.domain.User;
import com.novytech.model.service.ItemService;
import com.novytech.model.service.impl.DefaultServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 10:04
 */
public class ItemsDeleteCommand implements Command {
    private static final ItemsDeleteCommand instance = new ItemsDeleteCommand();
    private final Logger logger = LogManager.getLogger(ItemsDeleteCommand.class);
    private ItemsDeleteCommand(){}
    private final ItemService itemService = DefaultServiceFactory.getInstance().getItemService();

    public static ItemsDeleteCommand getInstance() {return instance;}


    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {
        if (user == null || user.getPermissionLevel() != PermissionLevel.ADMIN) {
            throw new InsufficientPermissionsException();
        }

        Integer id = Integer.parseInt(req.getParameter("id"));

        itemService.delete(id);

        return ManageItemsCommand.getInstance().execute(req, user);
    }
}