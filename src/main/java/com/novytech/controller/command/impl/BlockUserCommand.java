package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.model.domain.PermissionLevel;
import com.novytech.model.domain.User;
import com.novytech.model.service.ServiceFactory;
import com.novytech.model.service.UserService;
import com.novytech.model.service.impl.DefaultServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 23:38
 */
public class BlockUserCommand implements Command {
    private static final BlockUserCommand instance = new BlockUserCommand();
    private final Logger logger = LogManager.getLogger(BlockUserCommand.class);
    private final ServiceFactory sf = DefaultServiceFactory.getInstance();
    private BlockUserCommand(){}

    public static BlockUserCommand getInstance() {return instance;}


    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {

        if (user == null || user.getPermissionLevel() != PermissionLevel.ADMIN) {
            throw new InsufficientPermissionsException();
        }

        UserService userService = sf.getUserService();
        User adminUser = userService.findByLogin("admin");

        String idStr = req.getParameter("id");
        Integer id = Integer.parseInt(idStr);

        if (adminUser.getId().equals(id)) {
            return UserHomePageCommand.getInstance().execute(req, user);
        }

        userService.block(id);
        logger.info("User " + id + " has been blocked");

        return ManageUsersCommand.getInstance().execute(req, user);
    }
}
