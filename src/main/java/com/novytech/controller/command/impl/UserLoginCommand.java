package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.JspViews;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.model.dao.util.PasswordHelper;
import com.novytech.model.domain.User;
import com.novytech.model.service.impl.DefaultServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 20:51
 */
public class UserLoginCommand implements Command{
    private static final UserLoginCommand instance = new UserLoginCommand();
    private final Logger logger = LogManager.getLogger(UserLoginCommand.class);
    private UserLoginCommand() {}

    public static UserLoginCommand getInstance() {return instance;}


    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String passwordHash = PasswordHelper.getInstance().hash(password);

        if (login != null) {
            User authenticatedUser = DefaultServiceFactory.getInstance().getUserService().loginUser(login, passwordHash);
            if (authenticatedUser != null) {
                req.getSessionWrapper(true).setUser(authenticatedUser);

                return UserHomePageCommand.getInstance().execute(req, authenticatedUser);
            } else {
                logger.info("Failed to login as " + login + ": invalid login and/or password");
                return JspViews.GENERAL.LOGIN_PAGE;
            }
        } else {

            return JspViews.GENERAL.LOGIN_PAGE;
        }
    }
}
