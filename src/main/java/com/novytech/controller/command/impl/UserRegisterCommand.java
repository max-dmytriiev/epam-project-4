package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.exceptions.InvalidParameterValueException;
import com.novytech.controller.util.JspViews;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.controller.util.SessionWrapper;
import com.novytech.model.dao.util.PasswordHelper;
import com.novytech.model.domain.PermissionLevel;
import com.novytech.model.domain.User;
import com.novytech.model.domain.UserFactory;
import com.novytech.model.exceptions.UniqueException;
import com.novytech.model.service.UserService;
import com.novytech.model.service.impl.DefaultServiceFactory;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 15:53
 */
public class UserRegisterCommand implements Command {
    private static final Command instance = new UserRegisterCommand();
    private final Logger logger = LogManager.getLogger(UserRegisterCommand.class);

    private final UserService userService = DefaultServiceFactory.getInstance().getUserService();

    private UserRegisterCommand() {}

    static Command getInstance() {return instance;}

    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {
        try {
            String firstName = req.getParameter("first_name");
            String lastName = req.getParameter("last_name");
            String login = req.getParameter("login");
            String password = req.getParameter("password");

            String loginPatternStr = "^[a-zA-Z0-9_-]{3,15}$";
            Pattern loginPattern = Pattern.compile(loginPatternStr);

            if (login == null || !loginPattern.matcher(login).matches()) {
                logger.info("INVALID LOGIN");
                throw new InvalidParameterValueException("Invalid login");
            }

            String passwordPatternStr = "^[a-zA-Z0-9_-]{4,15}$";
            Pattern passwordPattern = Pattern.compile(passwordPatternStr);

            if (password == null || !passwordPattern.matcher(password).matches()) {
                logger.info("INVALID PASSWORD");
                throw new InvalidParameterValueException("Invalid password");
            }

            String passwordHash = PasswordHelper.getInstance().hash(password);
            User newUser = UserFactory.getInstance().createUserWithoutId(firstName, lastName, login, passwordHash, PermissionLevel.COMMON);

            boolean created = userService.create(newUser);

            if (created) {
                SessionWrapper sessionWrapper = req.getSessionWrapper(true);
                sessionWrapper.setUser(newUser);

                return UserHomePageCommand.getInstance().execute(req, newUser);
            }

        } catch (InvalidParameterValueException | UniqueException e) {
            req.setAttribute("error", e.getMessage());
            logger.error(e);
        }

        return JspViews.GENERAL.REGISTER_PAGE;
    }
}
