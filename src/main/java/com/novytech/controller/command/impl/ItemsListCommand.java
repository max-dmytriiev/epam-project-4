package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.JspViews;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.model.domain.Item;
import com.novytech.model.domain.User;
import com.novytech.model.service.impl.DefaultServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 10:18
 */
public class ItemsListCommand implements Command {
    private static final ItemsListCommand instance = new ItemsListCommand();
    private final Logger logger = LogManager.getLogger(ItemsListCommand.class);

    private ItemsListCommand() {}

    public static ItemsListCommand getInstance() {return instance;}

    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {
        int countPages = 0;
        int countItems = 0;
        String countStr = req.getParameter("countPages");
        if (countStr == null) {
            countItems = DefaultServiceFactory.getInstance().getItemService().count();
            int div = countItems / 10;
            int mod = countItems % 10;
            countPages = div;
            if (mod > 0) {
                ++countPages;
            }
        } else {
            countPages = Integer.parseInt(countStr);
        }

        String pageStr = req.getParameter("page");
        int page = 0;
        if (pageStr != null) {
            page = Integer.parseInt(pageStr);
        }

        List<Item> items = DefaultServiceFactory.getInstance().getItemService().findAll(page);

        req.setAttribute("items", items);
        req.setAttribute("page", page);
        req.setAttribute("countPages", countPages);
        return JspViews.ITEMS.LIST_PAGE;
    }
}