package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.JspViews;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.model.domain.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 20:51
 */
public class UserLogoutCommand implements Command{
    private static final UserLogoutCommand instance = new UserLogoutCommand();
    private final Logger logger = LogManager.getLogger(UserLogoutCommand.class);

    private UserLogoutCommand() {}

    public static UserLogoutCommand getInstance() {return instance;}

    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {
        if (user == null || !req.getSessionWrapper(false).getUser().getId().equals(user.getId())) {
            throw new InsufficientPermissionsException();
        }
        req.getSessionWrapper(false).invalidate();
        logger.info("Session of user: " + user.getLogin() + " has been invalidated");

        return JspViews.GENERAL.INDEX_PAGE;
    }
}
