package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.JspViews;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.model.domain.Order;
import com.novytech.model.domain.User;
import com.novytech.model.service.impl.DefaultServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 12:01
 */
public class OrdersListCommand implements Command {
    private static final OrdersListCommand instance = new OrdersListCommand();
    private final Logger logger = LogManager.getLogger(OrdersListCommand.class);

    private OrdersListCommand() {}

    public static OrdersListCommand getInstance() {return instance;}

    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {
        Integer userId = req.getSessionWrapper(false).getUser().getId();

        int countPages = 0;
        int countItems = 0;
        String countStr = req.getParameter("countPages");
        if (countStr == null) {
            countItems = DefaultServiceFactory.getInstance().getOrderService().countByUserId(userId);
            int div = countItems / 10;
            int mod = countItems % 10;
            countPages = div;
            if (mod > 0) {
                ++countPages;
            }
        } else {
            countPages = Integer.parseInt(countStr);
        }

        String pageStr = req.getParameter("page");
        int page = 0;
        if (pageStr != null) {
            page = Integer.parseInt(pageStr);
        }

        List<Order> orders = DefaultServiceFactory.getInstance().getOrderService().findOrdersForUserWithPage(userId, page);

        req.setAttribute("orders", orders);
        req.setAttribute("page", page);
        req.setAttribute("countPages", countPages);
        return JspViews.ORDERS.LIST_PAGE;
    }
}
