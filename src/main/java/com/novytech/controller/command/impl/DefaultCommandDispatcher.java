package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.command.CommandDispatcher;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.model.domain.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 15:16
 */
public class DefaultCommandDispatcher implements CommandDispatcher {
    private static final CommandDispatcher instance = new DefaultCommandDispatcher();
    private final Logger logger = LogManager.getLogger(DefaultCommandDispatcher.class);
    private DefaultCommandDispatcher(){}

    public static CommandDispatcher getInstance() {return instance; }

    enum CommandBinding {
        LIST_ORDERS(OrdersListCommand.getInstance()),
        PAY_ORDER(PayOrderCommand.getInstance()),
        CHECKOUT(CheckoutCartCommand.getInstance()),
        REMOVE_FROM_CART(RemoveFromCartCommand.getInstance()),
        ADD_TO_CART(AddToCartCommand.getInstance()),
        LIST_ITEMS(ItemsListCommand.getInstance()),
        DELETE_ITEM(ItemsDeleteCommand.getInstance()),
        EDIT_ITEM(ItemsEditCommand.getInstance()),
        EDIT_ITEM_FORM(ItemsEditFormCommand.getInstance()),
        CREATE_ITEM(ItemsNewCommand.getInstance()),
        NEW_ITEM_FORM(ItemsNewFormCommand.getInstance()),
        MANAGE_ITEMS(ManageItemsCommand.getInstance()),
        UNBLOCK_USER(UnblockUserCommand.getInstance()),
        BLOCK_USER(BlockUserCommand.getInstance()),
        MANAGE_USERS(ManageUsersCommand.getInstance()),
        LANGUAGE(LanguageCommand.getInstance()),
        HOME(UserHomePageCommand.getInstance()),
        LOGIN(UserLoginCommand.getInstance()),
        LOGOUT(UserLogoutCommand.getInstance()),
        REGISTER(UserRegisterCommand.getInstance());
        final Command command;

        CommandBinding(Command instance) {
            this.command = instance;
        }

        public Command getCommand() {
            return command;
        }
    }

    @Override
    public String executeRequest(RequestWrapper requestWrapper) throws InsufficientPermissionsException {
        User user = requestWrapper.getSessionWrapper(false).getUser();

        String commandString = requestWrapper.getParameter("command");
        Command command = CommandBinding.valueOf(commandString).getCommand();
        try {
            return command.execute(requestWrapper, user);
        } catch (InsufficientPermissionsException e) {
            logger.error(e);
            return UserHomePageCommand.getInstance().execute(requestWrapper, user);
        }

    }
}
