package com.novytech.controller.command.impl;

import com.novytech.controller.command.Command;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.JspViews;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.controller.util.SessionWrapper;
import com.novytech.model.domain.Item;
import com.novytech.model.domain.User;
import com.novytech.model.service.impl.DefaultServiceFactory;

import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 31.01.17, 10:57
 */
public class CheckoutCartCommand implements Command {
    private static final CheckoutCartCommand instance = new CheckoutCartCommand();

    private CheckoutCartCommand(){}

    public static final CheckoutCartCommand getInstance() {return instance;}


    @Override
    public String execute(RequestWrapper req, User user) throws InsufficientPermissionsException {
        SessionWrapper sw = req.getSessionWrapper(false);

        List<Integer> ids = sw.getCart();

        List<Item> cart = DefaultServiceFactory.getInstance().getItemService().findByListOfIds(ids);

        Float totalPrice = 0f;
        for (Item i: cart) {
            totalPrice += i.getPrice();
        }

        req.setAttribute("cart", cart);
        req.setAttribute("total", totalPrice);

        return JspViews.ITEMS.CHECKOUT_PAGE;
    }
}
