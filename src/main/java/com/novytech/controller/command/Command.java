package com.novytech.controller.command;

import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.model.domain.User;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 15:44
 */
public interface Command {
    String execute(RequestWrapper req, User user) throws InsufficientPermissionsException;
}
