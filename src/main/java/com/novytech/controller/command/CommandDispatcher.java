package com.novytech.controller.command;

import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.RequestWrapper;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 15:14
 */
public interface CommandDispatcher {
    String executeRequest(RequestWrapper requestWrapper) throws InsufficientPermissionsException;
}
