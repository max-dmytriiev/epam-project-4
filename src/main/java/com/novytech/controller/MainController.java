package com.novytech.controller;

import com.novytech.controller.command.CommandDispatcher;
import com.novytech.controller.command.impl.DefaultCommandDispatcher;
import com.novytech.controller.exceptions.InsufficientPermissionsException;
import com.novytech.controller.util.RequestWrapper;
import com.novytech.controller.util.impl.RequestWrapperImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 15:11
 */
public class MainController extends HttpServlet {

    private final transient Logger logger = LogManager.getLogger(MainController.class);
    private final transient CommandDispatcher commandDispatcher = DefaultCommandDispatcher.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String jspUrl;
        RequestWrapper requestWrapper = new RequestWrapperImpl(req);

        try {
            jspUrl = commandDispatcher.executeRequest(requestWrapper);
            req.getRequestDispatcher(jspUrl).forward(req, resp);
        } catch (InsufficientPermissionsException e) {
            logger.debug(e);
            resp.sendError(HttpServletResponse.SC_FORBIDDEN);
        } catch (Exception e) {
            logger.error(e);
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
