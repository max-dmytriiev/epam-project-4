package com.novytech.controller.exceptions;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 15:28
 */
public class RequestAttributeNotAllowedException extends RuntimeException {
    public RequestAttributeNotAllowedException(String message) {
        super(message);
    }
}
