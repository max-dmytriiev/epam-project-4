package com.novytech.controller.exceptions;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 16:05
 */
public class InvalidParameterValueException extends Exception {
    public InvalidParameterValueException(String message) {
        super(message);
    }
}
