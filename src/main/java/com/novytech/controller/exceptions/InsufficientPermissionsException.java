package com.novytech.controller.exceptions;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 15:33
 */
public class InsufficientPermissionsException extends Exception {
}
