package com.novytech.controller.util;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 15:22
 */
public interface RequestWrapper {
    String getParameter(String key);
    void setAttribute(String key, Object value);

    SessionWrapper getSessionWrapper(boolean toCreate);
}