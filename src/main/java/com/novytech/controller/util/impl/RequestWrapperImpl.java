package com.novytech.controller.util.impl;

import com.novytech.controller.util.RequestWrapper;
import com.novytech.controller.util.SessionWrapper;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 15:23
 */
public class RequestWrapperImpl implements RequestWrapper {
    private HttpServletRequest request;

    public RequestWrapperImpl(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setAttribute(String key, Object value) {
        request.setAttribute(key, value);
    }

    @Override
    public SessionWrapper getSessionWrapper(boolean toCreate) {
        return new SessionWrapperImpl(request.getSession(toCreate));
    }

    @Override
    public String getParameter(String key) {
        return request.getParameter(key);
    }
}
