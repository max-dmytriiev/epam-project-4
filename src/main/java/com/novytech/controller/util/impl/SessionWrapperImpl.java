package com.novytech.controller.util.impl;

import com.novytech.controller.util.SessionWrapper;
import com.novytech.model.domain.User;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 17:40
 */
public class SessionWrapperImpl implements SessionWrapper {
    private HttpSession session;

    public SessionWrapperImpl(HttpSession session) {
        this.session = session;
    }

    @Override
    public void invalidate() {
        session.invalidate();
    }

    @Override
    public void setUser(User user) {
        session.setAttribute("user", user);
    }

    @Override
    public User getUser() {
        return session==null?null:(User) session.getAttribute("user");
    }

    @Override
    public void setLocale(String language) {
        session.setAttribute("language", language);
    }

    @Override
    public void addToCart(Integer id) {

        if (session.getAttribute("cart") == null) {
            session.setAttribute("cart", new ArrayList<Integer>());
        }

        List<Integer> cart = (List<Integer>) session.getAttribute("cart");
        cart.add(id);

        session.setAttribute("cart", cart);
    }

    @Override
    public void removeFromCart(Integer id) {
        if (session.getAttribute("cart") == null) {
            session.setAttribute("cart", new ArrayList<Integer>());
        }

        List<Integer> cart = (List<Integer>) session.getAttribute("cart");
        for (int i = 0; i < cart.size(); i++) {
            if (cart.get(i) == id) {
                cart.remove(i);
                break;
            }
        }

        session.setAttribute("cart", cart);
    }

    @Override
    public List<Integer> getCart() {
        if (session.getAttribute("cart") == null) {
            session.setAttribute("cart", new ArrayList<Integer>());
        }

        return (List<Integer>) session.getAttribute("cart");
    }

    @Override
    public void emptyCart() {
        session.removeAttribute("cart");
    }
}
