package com.novytech.controller.util;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 17:52
 */
public interface JspViews {
    interface GENERAL {
        String REGISTER_PAGE = "authentication/register.jsp";
        String LOGIN_PAGE = "authentication/login.jsp";
        String INDEX_PAGE = "index.jsp";
        String BLOCKED_PAGE = "blocked.jsp";
    }

    interface ADMIN {
        String INDEX_PAGE="admin/index.jsp";
        String MANAGE_USERS_PAGE="admin/users.jsp";
        String MANAGE_ITEMS_PAGE="admin/items.jsp";
    }

    interface ITEMS {
        String CREATE_FORM = "items/new.jsp";
        String EDIT_FORM="items/edit.jsp";
        String LIST_PAGE="items/list.jsp";
        String CHECKOUT_PAGE="items/checkout.jsp";
    }


    interface ORDERS {
        String LIST_PAGE="orders/list.jsp";
    }
}
