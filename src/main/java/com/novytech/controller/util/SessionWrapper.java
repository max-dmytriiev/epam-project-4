package com.novytech.controller.util;

import com.novytech.model.domain.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Maksym Dmytriiev
 * @since 30.01.17, 17:35
 */
public interface SessionWrapper {
    void invalidate();
    void setUser(User user);
    User getUser();

    void setLocale(String language);

    void addToCart(Integer id);
    void removeFromCart(Integer id);
    List<Integer> getCart();
    void emptyCart();
}
